
local Music = {}

Music.tracks = { menu = "Music/menu-theme.ogg" }

function Music.Init(mainScene)
	Music.soundSource = mainScene:CreateComponent("SoundSource")
	Music.soundSource.soundType = SOUND_MUSIC
	
	
end

function Music.Play(track)
	local track = Music.tracks[track] or nil
	if track ~= nil then
		local music = cache:GetResource("Sound", track);
		music.looped = true
		Music.soundSource:Play(music)
	else
		print("Music track "..track.." not listed.")
	end
end

function Music.Stop()
	Music.soundSource:Stop()
end

return Music
